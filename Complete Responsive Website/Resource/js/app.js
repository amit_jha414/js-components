(function () {
    this.resApp = this.resApp || {};
    var ns = this.resApp;
    var nav;
    var header;
    var headertop;
    var howitworks;
    var cities;
    var cityImage;
    var iphoneLogo;
    var citiesTop;
    var signUpTop;
    var howitWorkstop;
    var windowWidth;
    var navList;
    var toggleButton;

    ns.init = function () {

        nav = document.getElementById('topnavBar');
        header = document.getElementById('topheader')
        howitworks = document.getElementById('howitworks');
        iphoneLogo = document.getElementById('iphonelogoimg');
        cities = document.getElementById('currentCities');
        cityImage = document.getElementsByName('cityimg');
        navList = document.getElementById('navlist');
        toggleButton = document.getElementById('togbtn');

        
        document.addEventListener('scroll', function (e) {
            ns.scroll(e);
        }, false);

        toggleButton.addEventListener('click', function (e) {
            ns.toggleNav(e);
            e.preventDefault();
        }, false);
        
        windowWidth = window.innerWidth;
        ns.displayHidenav();

    }

    ns.scroll = function (e) {
        
        headertop = header.getBoundingClientRect().top;
        citiesTop = cities.getBoundingClientRect().top;
        howitWorkstop = howitworks.getBoundingClientRect().top;
        
        if (document.body.scrollTop >= (headertop +document.body.scrollTop) + header.offsetHeight){
            nav.classList.add('sticky');
        } else {
            nav.classList.remove('sticky');
        }

        if (document.body.scrollTop >= (howitWorkstop +document.body.scrollTop) - howitworks.offsetHeight / 2 
            && document.body.scrollTop <= ( howitWorkstop +document.body.scrollTop) + howitworks.offsetHeight / 2) {
            iphoneLogo.classList.add('bounce');
        } else {
            iphoneLogo.classList.remove('bounce');
        }

        if (document.body.scrollTop >= (citiesTop +document.body.scrollTop) - cities.offsetHeight / 2
            && document.body.scrollTop <= (citiesTop + document.body.scrollTop )  + cities.offsetHeight / 2) {
            for (var i = 0; i < cityImage.length; i++) {

                cityImage[i].classList.add('fadein');
            }

        } else {

            for (var i = 0; i < cityImage.length; i++) {
                cityImage[i].classList.remove('fadein');
            }
        }

    }

    ns.toggleNav = function (e) {
        if (navList.classList.contains('active')) {
            navList.classList.remove('active');
        } else {
            navList.classList.add('active');
        }
    }

    ns.displayHidenav = function () {
        if (windowWidth < 767) {
            toggleButton.classList.add('active');
            navList.classList.remove('active');
        } else {
            toggleButton.classList.remove('active');
            navList.classList.add('active');
        }
    }

    ns.onResize = function () {
        windowWidth = window.innerWidth;
        ns.displayHidenav();
    }

})();


window.onload = function () {
    resApp.init();

}

window.onresize = function(){
    resApp.onResize();
}