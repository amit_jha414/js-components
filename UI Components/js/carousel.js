var slides = document.getElementsByClassName('slide');
document.getElementsByClassName('carousel-main')[0].addEventListener('mouseover',pause,false);
document.getElementsByClassName('carousel-main')[0].addEventListener('mouseleave',play,false);
document.getElementById('leftcontrol').addEventListener('click', leftclick, false);
document.getElementById('rightcontrol').addEventListener('click', rightclick, false);
var autoMode;
var slideList = document.getElementById('carousel');


function leftclick(e) {
    if (e) {
        e.preventDefault()
    };

    $("#carousel").animate({
        left: -780
    }, 1000, function () {
        $(this).find("li:last").after($(this).find("li:first"));
        $(this).css({
            left: 0
        });
    });

}

function rightclick(e) {
    if (e) {
        e.preventDefault()
    };


    $("#carousel").animate({
        left: 780
    }, 1000, function () {
        $("#carousel").find("li:first").before($("#carousel").find("li:last"));
        $(this).css({
            left: 0
        });
    });
}

function play(){
    
   autoMode = setInterval(function(){
        rightclick();
    },4000);
}

function pause(){
    
   clearInterval(autoMode);
}

play();

