(function () {
    this.dragdropApp = this.dragdropApp || {};
    var ns = this.dragdropApp;

    ns.load = function () {
        
        var dragitem = document.getElementById('drag');
        dragitem.addEventListener('mousedown', ns.initiate, false);
        dragitem.addEventListener('mouseup', ns.dragged, false);

        var dragitem1 = document.getElementById('drag1');
        dragitem1.addEventListener('mousedown', ns.initiate, false);
        dragitem1.addEventListener('mouseup', ns.dragged, false);

        this.item;
        this.reddragZone = document.getElementsByClassName('red-drop-zone')[0];
        this.greendragZone = document.getElementsByClassName('green-drop-zone')[0];
        
        document.addEventListener('mousemove', ns.dragging, false);
    }
    
    ns.dragging = function (e) {
        if (ns.item != null && ns.item != undefined) {
            ns.item.style.left = (e.pageX - ns.item.offsetWidth / 2) + 'px';
            ns.item.style.top = (e.pageY - ns.item.offsetWidth / 2) + 'px';
        }
    }

    ns.dragged = function (e) {
        
        if(ns.item.id == 'drag'){
            
            if(ns.greendragZone.getBoundingClientRect().left < ns.item.getBoundingClientRect().left
              &&ns.greendragZone.getBoundingClientRect().right > ns.item.getBoundingClientRect().right
              &&ns.greendragZone.getBoundingClientRect().top < ns.item.getBoundingClientRect().top
              &&ns.greendragZone.getBoundingClientRect().bottom > ns.item.getBoundingClientRect().bottom){
                
                ns.greendragZone.classList.add('dropped');
            }
        }
        
        else if(ns.item.id == 'drag1'){
             if(ns.reddragZone.getBoundingClientRect().left < ns.item.getBoundingClientRect().left
              &&ns.reddragZone.getBoundingClientRect().right > ns.item.getBoundingClientRect().right
              &&ns.reddragZone.getBoundingClientRect().top < ns.item.getBoundingClientRect().top
              &&ns.reddragZone.getBoundingClientRect().bottom > ns.item.getBoundingClientRect().bottom){
            
                ns.reddragZone.classList.add('dropped');
             }
        }
        ns.item = null;
    }


    ns.initiate = function (e) {
        ns.item = e.target;
    }

})();

dragdropApp.load();