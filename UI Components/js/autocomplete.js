(function () {
    this.autocompleteApp = this.autocompleteApp || {}
    var ns = this.autocompleteApp;

    ns.autocompleteClass = (function () {

        function autocompleteClass(element, array) {

            this.element = element;
            this.optionArray = array;
            var elementWidth = this.element.offsetWidth;
            var elementleft = this.element.getBoundingClientRect().left;
            var elementTop = this.element.getBoundingClientRect().top + this.element.offsetHeight ;
            this.div = document.createElement('div');
            this.div.style.cssText = 'position:absolute;width:'+elementWidth+'px;left:'+elementleft+'px;top:'+elementTop+'px;border:1px solid                 black;border-radius:5px';
            this.div.setAttribute('id','optionList');
            document.body.appendChild(this.div);
        }
        autocompleteClass.prototype.createOptionsList = function () {
            document.getElementById('optionList').style.display ='block';
            var suggetionList = autocompleteClass.prototype.getallmatchingwords.call(this);
            this.div.innerHTML ='';
            var ol = '';
        
            for (var j = 0; j < suggetionList.length; j++) {
                ol = ol + '<a href="#">' +suggetionList[j] +'</a>';
            }
            this.div.innerHTML= ol;
            ns.g = this.element;
            ns.f = this.div.childNodes;
            for(var i=0; i<this.div.childNodes.length; i++){
                this.div.childNodes[i].addEventListener('click',function(e){
                      ns.g.value = e.target.innerHTML;
                    document.getElementById('optionList').style.display ='none';
                    e.preventDefault();
                },false);
            }
        }
           
        autocompleteClass.prototype.getallmatchingwords = function () {

            var resArray = [];
            var value = this.element.value;
            var count = 0
            if (value) {
                for (var i = 0; i < this.optionArray.length; i++) {

                    if (this.optionArray[i].toLowerCase().indexOf(value.toLowerCase()) !== -1) {

                        resArray[count] = this.optionArray[i];
                        count = count + 1;
                    }
                }
            }
            return resArray;
        }
        return autocompleteClass;
    })();

    ns.autocomplete = function (element, array) {
        ns.t = new ns.autocompleteClass(element, array);
        element.addEventListener('input', function (e) {
            ns.t.createOptionsList();

        }, false);

    }

})();



var optionArray = ["ActionScript",
      "AppleScript",
      "Asp",
      "BASIC",
      "C",
      "C++",
      "Clojure",
      "COBOL",
      "ColdFusion",
      "Erlang",
      "Fortran",
      "Groovy",
      "Haskell",
      "Java",
      "JavaScript",
      "Lisp",
      "Perl",
      "PHP",
      "Python",
      "Ruby",
      "Scala",
      "Scheme",
      "HTML",
      "CSS"];
autocompleteApp.autocomplete(document.getElementById('txtautocomplete'), optionArray);
