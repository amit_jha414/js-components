(function () {
    this.pieapp = this.pieapp || {}
    var ns = this.pieapp;

    ns.pieclass = (function () {
        function piechart(element, values) {
            this.element = element;
            this.values = values;
            this.ctx = this.element.getContext('2d');
            this.element.addEventListener('mouseover', function (e) {
                var x = e.pageX - e.target.offsetLeft;
                var y = e.pageY - e.target.offsetTop;



            }, false);
        }

        piechart.prototype.calculateArcLength = function () {
            var arcLength = [];
            var valArray = this.values;
            var totalsum = 0
            for (var i = 0; i < valArray.length; i++) {
                totalsum += valArray[i].x;
            }
            for (var i = 0; i < this.values.length; i++) {
                var obj = {
                    y: valArray[i].y,
                    x: (valArray[i].x / totalsum),
                    color: valArray[i].color
                };
                arcLength.push(obj);
            }

            return arcLength;
        }

        piechart.prototype.drawPiechart = function () {
            var ctx = this.ctx;
            ctx.imageSmoothingEnabled = true;
            var lastangle = 0;
            var k = piechart.prototype.calculateArcLength.call(this);
            for (var i = 0; i < k.length; i++) {
                ctx.save();
                ctx.fillStyle = k[i].color;
                ctx.beginPath();
                ctx.moveTo(this.element.width / 2, this.element.height / 2);
                ctx.arc(this.element.width / 2, this.element.height / 2, 200, lastangle * 2 * Math.PI, (lastangle + k[i].x) * 2 * Math.PI)
                ctx.fill();
                ctx.closePath();
                ctx.restore();
                ctx.save();
                ctx.translate(this.element.width / 2, this.element.height / 2);
                ctx.fillStyle = 'black';
                ctx.rotate(lastangle * 2 * Math.PI);
                ctx.textAlign = 'left';
                ctx.font = "15px sans serif";
                ctx.fillText(k[i].y + ' ' + Math.floor(k[i].x * 100) + '%', (this.element.width / 2) - 140, (this.element.height / 18));
                ctx.restore();
                lastangle += k[i].x;

            }
        }

        return piechart;
    })();

    ns.piechart = function (ele, values) {
        var x = new ns.pieclass(ele, values);
        x.drawPiechart();
    }

})();

window.onload = function () {
    var values = [{
            x: 10,
            y: 'V1',
            color: '#ebcfcf'
    }, {
            x: 20,
            y: 'V2',
            color: '#c8ebc8'
    }, {
            x: 10,
            y: 'V3',
            color: '#9393b5'
    }, {
            x: 25,
            y: 'V4',
            color: '#d5e0a8'
    },
        {
            x: 35,
            y: 'V5',
            color: '#cdf03a'
    },
        {
            x: 13,
            y: 'V6',
            color: '#fcb1b1'
    },
        {
            x: 10,
            y: 'V7',
            color: '#9d6a6a'
    }, {
            x: 17,
            y: 'V8',
            color: '#6a3b72'
    }, {
            x: 22,
            y: 'V9',
            color: '#ffe93e'
    }];
    pieapp.piechart(document.getElementById('piechart'), values);
}