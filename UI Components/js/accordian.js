(function () {
    this.accordianapp = this.accordianapp || {};
    var ns = this.accordianapp;

    ns.accordian = (function () {
        function accordian() {
            this.acoordianControls = document.getElementsByClassName('accordian-control');
            this.accordianContent = document.getElementsByClassName('accordian-content');
            for (var i = 0; i < this.acoordianControls.length; i++) {
                this.acoordianControls[i].addEventListener('click', function (e) {
                    ns.accordian.toggleContent(e);
                    e.preventDefault();
                }, false);
            }
        };
        accordian.prototype.toggleContent = function (e) {
            var t = e.target;
            if (t.nextElementSibling.classList.contains('active')) {
                t.nextElementSibling.classList.remove('active')
            } else {
                t.nextElementSibling.classList.add('active')
            }
            ns.accordian.collapserest(t.nextElementSibling);
        };
        accordian.prototype.collapserest = function (currentTab) {

            for (var i = 0; i < this.accordianContent.length; i++) {

                if (this.accordianContent[i] !== currentTab) {
                    this.accordianContent[i].classList.remove('active');
                }
            }
        }
        return new accordian;

    })();

    ns.collapse = (function () {
        function collapse() {
            this.collaspecontrol = document.getElementById('collapse-control');
            this.collapsecontent = document.getElementById('collapse-content');
            this.status = 0;
            this.collaspecontrol.addEventListener('click', function (e) {
                ns.collapse.toggleContent();
                e.preventDefault();
            }, false);

        };

        collapse.prototype.toggleContent = function () {

            if (this.collapsecontent.classList.contains('active')) {
                this.collapsecontent.classList.remove('active');

            } else {
                this.collapsecontent.classList.add('active');
            }
        };

        collapse.prototype.init = function () {

            this.app = new accordianapp.collapse();
        }
        return new collapse();
    })();

})();
