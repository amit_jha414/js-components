(function ($) {
    this.wizardUtility = this.wizardUtility || {};
    var ns = this.wizardUtility;

    ns.wizardClass = (function () {
        var count = 0;

        function wizardClass(inpArray, leftcontrol, rightcontrol, header) {
            this.input = inpArray;
            this.lftcntrl = leftcontrol;
            this.rgtcntrl = rightcontrol;
            this.head = header;
            var t = this;
            wizardClass.prototype.init.call(t);
            $('#' + leftcontrol).click(function (e) {
                e.preventDefault();
                wizardClass.prototype.previousClick.call(t);
            });

            $('#' + rightcontrol).click(function (e) {
                e.preventDefault();
                wizardClass.prototype.nextClick.call(t);
            });
        }
        wizardClass.prototype.init = function () {
            var t = this;
            $('#wu-view').css('opacity', '0');
            $('#wu-view').load(t.input[0].url, function () {
                t.input[0].clbfn();
                $('#' + t.head).text(t.input[0].step);
            });
            $('#wu-view').animate({
                opacity: 1
            }, 500);
        }

        wizardClass.prototype.previousClick = function () {
            var t = this;
            $('#wu-view').css('opacity', '0');
            count -= 1;
            if (count < 0) {
                count = 0;
            }
            $('#wu-view').load(t.input[count].url, function () {
                t.input[count].clbfn();
                $('#' + t.head).text(t.input[count].step);
            });
            $('#wu-view').animate({
                opacity: 1
            }, 500);

        }

        wizardClass.prototype.nextClick = function () {
            var t = this;
            $('#wu-view').css('opacity', '0');
            count += 1;
            if (count > this.input.length - 1) {
                count = this.input.length - 1;
            } else {
                $('#wu-view').load(t.input[count].url, function () {
                    t.input[count].clbfn();
                    $('#' + t.head).text(t.input[count].step);

                });
            }
            $('#wu-view').animate({
                opacity: 1
            }, 500);
        }
        return wizardClass;
    })();

    ns.wizard = function (a, l, r, h) {
        var k = new ns.wizardClass(a, l, r, h);
    }
})(jQuery);