//Demo
window.onload = function () {

    var finalobject = {};
    var inparr = [
        {
            step: 'Sign Up',
            url: '../html/step1.html',
            clbfn: page1
        },
        {
            step: 'Payment',
            url: '../html/step2.html',
            clbfn: page2
        },
        {
            step: 'Review',
            url: '../html/end.html',
            clbfn: end
        }
    ]
    //callback funtions
    function page1() {
        $('#btnsubmit').click(function () {
            finalobject.email = $('#email').val();
            finalobject.password = $('#pwd').val();
            $('#rgtcntrl').trigger('click');
        });
    }

    function page2() {

        $('input[type=checkbox]').click(function (e) {
            finalobject.selectedPaymentMethod = e.target.value;
        });
    }
    //flow end callback function
    function end() {
        $('#emaildisp').append(finalobject.email);
        $('#pt').append(finalobject.selectedPaymentMethod) ;
    }
    //param1:array of input, param2:previous button id, param3: next button id , param4: header ID
    wizardUtility.wizard(inparr, 'lftcntrl', 'rgtcntrl','stepheader');

}