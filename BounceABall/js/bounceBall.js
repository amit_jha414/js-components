(function () {
    this.bounceBall = this.bounceBall || {}
    var ns = this.bounceBall;
    var cv = document.getElementById('gameCanvas');
    var ctx = cv.getContext('2d');
    var ballX = 0;
    var ballY = 75;
    var ballXdir = 5;
    var ballYdir = 5;
    var xratio = cv.offsetWidth / cv.width;
    var yratio = cv.offsetHeight / cv.height;
    var paddlex = 0;
    var paddley = 0;
    var player1 = 0;
    var player2 = 0;
    var p1 = document.getElementById('player1');
    var p2 = document.getElementById('player2');
    ns.init = function () {

        cv.addEventListener('mousemove', function (e) {
            paddlex = e.clientX / xratio;
            paddley = e.clientY / yratio;
        }, false);
        setInterval(function () {
            ns.drawFrames();
            ns.move();
        }, (1000 / 50));


    }

    ns.drawFrames = function () {
        ns.drawRectangle(0, 0, cv.width, cv.height, 'black');
        ns.drawRectangle(paddlex, 2, 40, 5, 'white');
        ns.drawRectangle(paddlex, cv.height - 7, 40, 5, 'white');
        ns.drawCircle(ballX, ballY, 5, 'red');
    }
    ns.move = function () {
        ballX = ballX + ballXdir;
        ballY = ballY + ballYdir;

        if (ballX > cv.width) {
            ballXdir = ballXdir * (-1);
        } else if (ballX < 0) {
            ballXdir = ballXdir * (-1);
        }

        if (ballY < 0) {
            ballY = 75;
            ballX = 0;
            player1 = player1 - 10;
            p1.innerHTML = player1;
        } else if (ballX > paddlex - 5 && ballX < (paddlex + 45) && ballY == 10) {
            ballYdir = ballYdir * (-1);
            player1 = player1 + 10;
            p1.innerHTML = player1;
        } else if (ballX > paddlex - 5 && ballX < (paddlex + 45) && ballY == cv.height - 10) {
            ballYdir = ballYdir * (-1);
            player2 = player2 + 10;
            p2.innerHTML = player2;
        }

        if (ballY > cv.height) {
            ballY = 75;
            ballX = 0;
            player2 = player2 - 10;
            p2.innerHTML = player2;
            val = 0;
        }

    }
    ns.drawRectangle = function (x, y, width, height, color) {
        ctx.fillStyle = color;
        ctx.fillRect(x, y, width, height);
    }

    ns.drawCircle = function (xCenter, yCenter, radius, color) {
        ctx.fillStyle = color;
        ctx.beginPath();
        ctx.arc(xCenter, yCenter, radius, 0, Math.PI * 2, true);
        ctx.fill();
    }
})();

window.onload = function () {
    bounceBall.init();
}
