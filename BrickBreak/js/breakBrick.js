(function () {
    this.brickBlast = this.brickBlast || {}
    var ns = this.brickBlast;
    var cv = document.getElementById('gameCanvas');
    var ctx = cv.getContext('2d');
    var score = document.getElementById('score');
    var levelelm = document.getElementById('level');
    var level = 1;
    var scrVal = 0
    var ballX = 0;
    var ballY = 75;
    var ballXdir = 5;
    var ballYdir = 5;
    var xratio = cv.offsetWidth / cv.width;
    var yratio = cv.offsetHeight / cv.height;
    var paddlex = 0;
    var paddley = 0;
    var bricks = [];
    var brickHeight = 10;
    var bricksXlen = 10;
    var bricksYlen = 3;
    var bricksWidth = cv.width / bricksXlen;
    var bricksMargin = 2;
    var framesPerSecond = 50;
    var run;

    ns.init = function () {

        ns.constructBricks();
        levelelm.innerHTML = level;
        cv.addEventListener('mousemove', function (e) {
            paddlex = e.clientX / xratio;
            paddley = e.clientY / yratio;
        }, false);

       run =  setInterval(function () {

            ns.drawFrames();
            ns.move();
            ns.checkGameOver();
        }, (1000 / framesPerSecond));


    }

    ns.drawFrames = function () {
        ns.drawRectangle(0, 0, cv.width, cv.height, 'black');
        ns.drawRectangle(paddlex, cv.height - 7, 50, 5, 'white');
        ns.drawCircle(ballX, ballY, 5, 'red');
        ns.drawBricks();
    }
    ns.move = function () {
        ballX = ballX + ballXdir;
        ballY = ballY + ballYdir;

        if (ballX > cv.width) {
            ballXdir = ballXdir * (-1);
        } else if (ballX < 0) {
            ballXdir = ballXdir * (-1);
        }

        if (ballY <= 5) {
            ballYdir = -ballYdir;

        } else if (ballY < brickHeight * bricksYlen && ballY > 0) {
            var x = parseInt((ballX + bricksWidth) / (bricksWidth + bricksMargin) - 0.70);
            var y = parseInt((ballY + 1) / (brickHeight + 2));

            if (bricks[y][x]) {
                scrVal = scrVal + 100;
                score.innerHTML = scrVal;
                bricks[y][x] = false;
                ballYdir = -ballYdir;
            }

        } else if (ballX > paddlex - 10 && ballX < (paddlex + 55) && ballY == cv.height - 10) {
            ballYdir = ballYdir * (-1);
        } else if (ballY > cv.height) {
            //ballYdir = -ballYdir;
            ns.resetGame();
        }

    }
    ns.drawRectangle = function (x, y, width, height, color) {
        ctx.fillStyle = color;
        ctx.fillRect(x, y, width, height);
    }

    ns.drawCircle = function (xCenter, yCenter, radius, color) {
        ctx.fillStyle = color;
        ctx.beginPath();
        ctx.arc(xCenter, yCenter, radius, 0, Math.PI * 2, true);
        ctx.fill();
    }
    ns.drawBricks = function () {
        for (var y = 0; y < bricksYlen; y++) {
            for (var x = 0; x < bricksXlen; x++) {
                if (bricks[y][x]) {
                    var xPos = x * bricksWidth + bricksMargin;
                    var yPos = y * brickHeight + bricksMargin;
                    var width = bricksWidth - bricksMargin;
                    var height = brickHeight - bricksMargin;
                    ns.drawRectangle(xPos, yPos, width, height, 'green');
                }
            }
        }
    }

    ns.constructBricks = function () {
        for (var y = 0; y < 15; y++) {
            bricks[y] = [];
            for (var x = 0; x < 15; x++) {
                bricks[y][x] = true;
            };
        };
    }
    ns.checkGameOver = function () {
        if (scrVal == 3000) {
            var c = confirm("Congrats! you have won the game");
            if (c) {
                scrVal = 0;
                ns.resetGame();
                ns.increaseLevel();
            }
        }
    }
    ns.resetGame = function () {
       
        setTimeout(function () {
            ballY = 75;
            ballX = 0;
            ns.constructBricks();
            scrVal = 0;
            score.innerHTML = scrVal;
            
        }, 1000);
    }
    ns.increaseLevel = function(){
        level = level +1;
        levelelm.innerHTML = level;
        framesPerSecond = framesPerSecond +10;
        clearInterval(run);
        run =  setInterval(function () {

            ns.drawFrames();
            ns.move();
            ns.checkGameOver();
        }, (1000 / framesPerSecond));
    }
})();

window.onload = function () {
    brickBlast.init();
}
