# README #

Basic UI application based on JavaScript, HTML and CSS

### What is this repository for? ###

* This repository is created to add any projects which is mainly created in basic javascript and using its libraries.


### Projects ###
* Drag an element
* Drop in an element
* Carousel(used Jquery Animate)
* Accordion
* Sidebar navigation
* Image zoom gallery
* Autocomplete text box(Jquery UI inspired implemented in native javascript)
* Modal Dialogue Box
* Bounce Ball(Game written in native JavaScript and canvas element)
* Brick Blast(Game written in native JavaScript and canvas element)
* Expand and Collapse (Used in dropdowns)
* A fully functional responsive website(Designed in HTML5 ,CSS3 and JavaScript)
* pie chart using HTML 5 canvas element.
* flow wizard project.

### How do I get set up? ###

* Download the app(repository). This will copy the entire folder structure in your local machine. Then just browse the Html file in browser.

### Dependencies
* These projects may not work well for IE below version 9 because it do not support HTML5 and CSS3.
* Browser should have JavaScript enabled.